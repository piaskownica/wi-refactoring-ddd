package io.pillopl.dietary;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerOrderGroupRepository extends JpaRepository<CustomerOrderGroup, Long> {
}



